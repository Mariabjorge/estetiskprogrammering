## MiniX6

###### Runme
https://Mariabjorge.gitlab.io/estetiskprogrammering/MiniX6/

###### code:
https://gitlab.com/Mariabjorge/estetiskprogrammering/-/blob/main/MiniX6/sketch.js

In this week’s MiniX we were told to make a mini game based on a class-based and object-oriented sketch. We were also told that this MiniX was going to be the hardest one yet, which is something that I have to agree with. During my programming process this week, I had a hard time getting my game to turn out exactly the way I wanted. I therefore ended up getting a lot of inspiration for my game from Winnie's Tofu game. The task of making a simple mini-game made me think of the games I played as a child. The nostalgic feeling I got from thinking about these games inspired me to come up with an idea for a game. I wanted to make something similar to the games I played myself, which meant naive and cute and kind off on the surface. That’s why my game is just a cute bunny, eating carrots in a field.

#### How does/do your game/game objects work? Describe how you program the objects and their related attributes, and the methods in your game.
The carrots is constructed as a class and each carrot is an object instance. The carrots have properties such as speed, size and positions. The carrots also include some behavior which is show() and move(). The goal of my game is for the bunny  to eat as much carrots as possible, and the way you do this is by dragging your mouse (mouseX, mouseY) to the carrots. In an attempt to not make my game too easy, I made both the speed and the minimum amount of carrots that appear on the screen to be 15. The combination of amount and speed makes it impossible for the bunny to eat all the carrots on the screen and therefor makes the game a little bit harder. The game is over if you miss the same amount of carrots as you have eaten.

#### Draw upon the assigned reading, are the characteristics of object-oriented programming and the wider implications of abstraction?
In the Aesthetic Programming Handbook its written “With the abstraction of relations, there is an interplay between abstract and concrete reality.” (p.161 - Soon & Cox, 2020). In object-oriented programming, you have the opportunity to create an object based on your personal opinion. In my game, for example, I chose to pick up some aspects inspired by the real world, such as rabbits eating carrots. However, I have given the carrots some properties and behavior that they do not have in reality, such as for example to float in the air. I think it's interesting to think about how one can move quite far from how things work/are/behave in reality where people still want to draw a comparison to something from the real world. This can be a good thing because it can, for example, help the player to understand how he / she should play the game by drawing a connection between certain types of characteristics or behaviors and certain objects. Since the programming is based on the programmer's own thoughts and views on the world, this will affect how the game turns and can create implications. If the programmer e.g. have discriminatory views and attitudes, this can play out in the game, and be harmful to others.

#### Connect your game project to a wider cultural context, and think of an example to describe how complex details and operations are being “abstracted”?
As mentioned earlier in the text I would consider my game as outdated, with it being inspired by the games I played as a child. Initially when making this game, I had a desire to create a reference to the makeup industry and their animal testing. The way I wanted to do this would be by creating more objects that would be different makeup products that also came floating in the air along with the carrots. To win the game you would have to eat only the carrots, because if the bunny ate the makeup it would die. This was something I tried to achieve but unfortunately without success. If I had done this, it would have been much more culturally relevant than what I ended up with. I think that even if it had not been obvious that it was animal testing I was referring to, I think that many would still understand the reference only based on the simple objects and their properties and behavior in interaction with the bunny.

Reference list:
- Winnie Soon & Geoff Cox; 'Aesthetic --  Programming: A Handbook of Software Studies', 2020

![](picture.png)
