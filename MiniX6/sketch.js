let bunnySize = {
  w:155,//global value
  h:155
};
let bunny;
let carrots;
let bunnyPosY;
let bunnyPosX;
let mini_height;
let min_carrot = 15;  //minimum carrots on the screen
let carrot = [];//array
let score =0, lose = 0;
let keyColor = 255;//white

function preload(){
  bunny = loadImage("jumping_bunny.gif");//bunny
  carrots = loadImage("carrot.gif");//carrot
  field = loadImage("field.jpg");//background
}

function setup() {
  createCanvas(windowWidth, windowHeight);
  bunnyPosY = height/2;
  bunnyPosX = width/2;
  mini_height = height/2;
}
function draw() {
  //this is the order the program is running in
  background(field);
  fill(keyColor);
  displayScore();
  checkCarrotNum(); //available carrots, naming a function for later (making the code more manageble)
    showCarrot();
  image(bunny, mouseX-40, mouseY-40 ,120,100);//the mouse and the size of the bunny
  //image(bunny, 0, bunnyPosY,120,100);


  checkEating(); //scoring
  checkResult();

}

function checkCarrotNum() { //telling the computer to make new carrots - (by checking the number of carrots on the screen)- conditional statement
  if (carrot.length < min_carrot) {//carrot.lenght is the amout of carrots on the screen
    carrot.push(new Carrot()); //push is a generative function - creates a new object instance -> go through the class/object
  }
}

function showCarrot(){ //making sure all the objects are displayed on the screen through loop
  for (let i = 0; i <carrot.length; i++) {//showing the carrots on the screen by adding new ones through the array.
    carrot[i].move(); //telling carrots to move
    carrot[i].show(); //making carrots show
  }
}

function checkEating() { //if carrot position moves beyond the screen, it must be deleted
  //calculate the distance between each carrot
  for (let i = 0; i < carrot.length; i++) {
      //decaliring and assigning/definig local variable
    let d = int(dist(mouseX,mouseY,carrot[i].pos.x, carrot[i].pos.y));
//conditional (else if) statement - used to check if something in the program is true
    if (d < bunnySize.w/4) { //close enough as if eating the tofu
      score++;
      carrot.splice(i,1);//deletes the carrot that have been eaten
    }else if (carrot[i].pos.x < 1) { //carrot have moved out of the canvas (0 on x ax) = bunny missed the carrot
      lose++;
      carrot.splice(i,1); //splice deletes, i = which one to delete in array, 1 = how many forward in the array to delete
      //deletes 1 i form the array for each time carrot(i) goes out of the canvas
      //If the carrots going out of the canvas werent deleted (spliced), they would still be in the program -
      //this would lead to the for-loop stoppoing after the first 15 carrots because i would be = carrot.lengt (15),
      //and not 15 - 1 = 14,. The splice function is what makes it possible for new carrots to always move into the canvas
      //because i always will be less than carrot.lenght
    }
  }


}

function displayScore() {//the text telling the score
    fill(251, 149, 1);
    textSize(25);
    text('Eaten '+ score + " carrots", 10, height/10);
    text('Wasted ' + lose + " carrots", 7, height/7);
    fill(keyColor);
    text('Play the game by moving the bunny around with your mouse to make him eat the carrots',
    20,30);
}

function checkResult() {
  //checking if the bunny have wasted more carrots than it has eaten or if it (in the beginning) has wasted more than 10 carrots - if so, the loop stops and the text saying that you have lost the game appear
  if (lose > score && lose > 10) {
    fill(255,0,0);//red
    textSize(30);
    text("You missed too many carrots... bunny is now hungry :(", width/4.5, height/2);
    noLoop();//the loop stops when you lose the game
  }

  // Prevent default browser behaviour
  // attached to key events.
  return false;
 }


//function keyPressed() {
//if (keyCode === UP_ARROW) {
//bunnyPosY-=40;
//} else if (keyCode === DOWN_ARROW) {
  //bunnyPosY+=50;
//} else if (keyCode === LEFT_ARROW) {
//  pbunnyPosX-=50;
//} else if (keyCode === RIGHT_ARROW) {
  //bunnyPosX+=50;
//}
//}
