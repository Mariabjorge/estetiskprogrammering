class Carrot{
  constructor(){
    this.speed = floor(15);
    this.size = floor(random(35,35));
    this.pos = new createVector(width+5, random(12, height/1.7));//A vector is an entity that has both magnitude and direction
  }
  move(){
    this.pos.x -= this.speed; //this.pos.x = this.pos.x - this.speed.
  }
  show(){ //telling the computer to show the carrots
    push();//generative function
    image(carrots,this.pos.x, this.pos.y, this.size, this.size);

  }
}
