//buttons
let textimg;
let buttonReject;
let buttonAllow;
let capture;
//array, string of words is being assigned to the varable 
let reject = ["Error", "Something went wrong", "Try again", "Try refreshing the page", "I think you made a mistake?", "wrong choice", "This is not working", "What's the worst that can happen", "Try again!", "I think you missed", "Are you still here?!", "Almost there!", "Are you stupid, just press green"]

//images
function preload(){
blurred=loadImage("blurred.jpg")
textimg=loadImage("text.png")
emblem=loadImage("emblem.png")
check=loadImage("check.jpg")
tekst=loadImage("tekst.png")
}

function setup() {
  createCanvas(1000,1000);
    frameRate(20);
    background(blurred);

//Camera
capture = createCapture(VIDEO);
capture.size(640, 480);
capture.hide();//hides the video before its getting called on

//front page
  image(textimg,200,200)
  image(emblem,50,150,100,100)


//reject button
  buttonReject = createButton('Reject all terms and conditions');
  buttonReject.style('font-size','8px')
  buttonReject.style("background","red")
  buttonReject.size(220,100);
  buttonReject.position(200,650);
  buttonReject.mousePressed(change1);


//Allow button
  buttonAllow = createButton('Accept all terms and conditions :)');
  buttonAllow.style('font-size','20px')
  buttonAllow.style("background", "green")
  buttonAllow.style("color","white")
  buttonAllow.size(250,100);
  buttonAllow.position(415,650);
  buttonAllow.mousePressed(change2);


// Text
textSize(18);
fill((35, 22, 8));
text('To continue you have to accept our terms and conditions:',200,195);
//terms and conditions


}
//if you press reject button
function change1() {
  background(255)
  fill(255,0,0);
  textSize(50);
  textStyle(BOLD);
  buttonReject = text(random(reject),200, 400);//random word from array


}
//if you press accpet button
function change2() {
  background(255)
  fill(255,0,0);
  textSize(24);
  textStyle(BOLD);
  text('Thank you very much for the donation' , 10, 90);
  image(check,0,400,500,400)
  image(tekst,0,100,500,200)
  image(emblem,56,700,35,35);
  //taking picture
  image(capture,500,400,400,400);
  //hiding the buttons when you press the "accept" button

  buttonReject.hide();
  buttonAllow.hide();

}
