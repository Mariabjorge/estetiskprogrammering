## MiniX7

###### Runme
https://Mariabjorge.gitlab.io/estetiskprogrammering/MiniX7/

###### code:
https://gitlab.com/Mariabjorge/estetiskprogrammering/-/blob/main/MiniX7/sketch.js

#### The MiniX I have reworked

In weeks MiniX I decided to revisit my MiniX4. There are several reasons for why I chose to revisit this particular MiniX. The main reason being that I was not very satisfied with how this MiniX turned out the first time around. I found it very difficult to make my coding work when making this MiniX. I therefore focused on “low floors” and not making anything that I did not understand myself.
Another reason for why I picked this MiniX is because I find the theme “data capture” very interesting and would like to dig deeper into the topic. I also thought that with this being a pretty open assignment, it would leave room to make all sorts of changes.

#### What I have changed and why

When I first made this MiniX, I initially wanted to include several DOM elements in my program such as both buttons and VIDEO. I tried making this work many times, without success. This was thus my main focus in this reworked MiniX.

 I used the old MiniX and its concept the starting point of my new MiniX. With this base, I added a number of changes. In an attempt to make the front page look more like a realistic website, I chose to insert two images where one was a blurred background and the other a text block. The text is written in Russian letters. The reason I chose to do this was because I thought this would emphasize the point that no one reads terms and conditions anyway, so it might as well be written in Russian. In the upper left corner, I chose to insert one image of the anonymous emblem as a reference to hackers and data capture. This reference was meant to put a more shady feel to the page. I chose to keep the two buttons I had already made, but I changed the outcome if you press them. I made a list of different words that will pop up in randomly order if you press the Reject button. If you press Accept, a picture of a check of a check that also has the anonymous emblem on it will appear, in addition to a picture of you taken from your front camera. I used the syntax “image(capture,500,400,400,400);”  under my own function “function change2()” to make the image pop up after you press the button. A message will also appear stating that you have now given away your personal information.

In a documentary Harvard professor Shoshana Zuboff, talks about how we believe we have control over the personal information we give out about ourself but in reality that is not the case. In the documentary she says “ We provide personal information, but the information we provide is the least important part of the information that they collect about us”. This saying inspired me when coming up with a concept for my MiniX. In my MiniX I wanted to shed some light on the fact that we have almost no control over our personal data, and if someone was to get a hold of it they would have huge control over us.

As said earlier, I wanted to use this MiniX as an opportunity for me to revisit some of the challenges I faced when first making this MiniX, to try and see if I could make it work this time around. I find this chapter kind of hard, but at the same time super interesting. So I was glad when I finally learned how I could make the syntaxes work together in the way I wanted.

![](Front.page.png)
![](reject.png)
![](accept.png)

#### What is the relation between aesthetic programming and digital culture?

When I first started in this class, I believed aesthetic programming to be just about creating something that would be aesthetically pleasing to look at. It turned out that this was not the case at all. Aesthetic programming can be used as a tool to change the world for the better. As it aims to be easily accessible for everyone to use, it makes room for diversity in the digital world. It makes it possible for many different types of people to use this form of programming and together ask questions about and challenge the norms of today's society and digital culture.


####	How does your work demonstrate the perspective of aesthetic programming (look at the Preface of the Aesthetic Programming book)?

My work demonstrates the perspectives of aesthetic programming in that it is about creating an impact. My MiniX does not aim to be pretty to look at, but it tries to convey a message that will influence the user.



###### References:

-	Winnie Soon & Geoff Cox; 'Aesthetic Programming: A Handbook of Software Studies', 2020
-	https://www.youtube.com/watch?v=hIXhnWUmMvw
