## MiniX5

###### Runme
https://Mariabjorge.gitlab.io/estetiskprogrammering/MiniX5/

###### code:
https://gitlab.com/Mariabjorge/estetiskprogrammering/-/blob/main/MiniX5/sketch.js

#### What are the rules in your generative program?

In my program I wanted to include the following rules:
1.	Draw circles in random sizes close to each other from the center of the canvas

2.	Make the stroke of the circles in random colors

####	The way my program preforms over time:
In the beginning of the program it is just a random colored and sized outline of a circle that appear somewhere in the center of the canvas. After this a new circle appear close to the first one at a random position between +10 pixels and -10 pixels at the x- and y-axis. As time goes on and increasingly more circles are filling the canvas, the program is starting to look like something else than what it started out as. It remindes me of YouTube videos I watched as a child when you have to look in the center of the spiral for a certain amount of time and when you look away your surroundings will look trippy and weird.

In the following pictures you can see how the program looks at first compared to when its been running for a while:  
![](spiral_nr.1.png)
![](spiral_nr.2.png)

#### How do the rules produce emergent behavior?
The rules produce emergent behavior by obviously having a big influence on how the program will perform and turn out. Because I have chosen to make the stroke color random, this means that the circles always will be in a random color. The circles will also always appear in the center of the canvas close to another circle in a random size between -10 and 10 pixels.
There is also a lot of factors that I have no control over, such as if the circles will grow or shrink, moving closer to or further from the center-point of the canvas. How the program is deciding to run in the beginning can have an influence on how the way it’s being perceived.

#### What role do rules and processes have in your work?
Rules and processes plays a big role in my work. I started out with a vision in my head that I wanted to bring to life, by creating some rules that could make this happen. Once I had typed in the rules, the program didn’t look quite the way I expected it to. I then had to go through a process where I made changes and adjustments to make the program appear and perform in a pleasing way. Along the way I noticed how just the smallest adjustments could change the whole artwork and make it something completely different.  

#### Draw upon the assigned reading, how does this MiniX help you to understand the idea of “auto-generator” (e.g. levels of control, autonomy, love and care via rules)? Do you have any further thoughts on the theme of this chapter?
In advance of typing in my rules, I had pictured how I imagined the program would look like. It turned out that this was only accurate to a certain extend. When looking at how the program performed over time it was a lot more dynamic and ever-changing than I had pictured it in my mind. The computer had made it its own, leaving me in control with only a part of the outcome.

I thought this MiniX was a fun and interesting way of exploring with different syntaxes and make something that could be considered art. It made me see how art can be so much more than what comes to mind when we think of traditional art
