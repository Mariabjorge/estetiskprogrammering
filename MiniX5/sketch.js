let x;//the colored stroke
let y;

let r;//the colors
let b;
let g;

let space = 10;//space between stroke

function setup() {
  createCanvas(windowWidth, windowHeight);
  background(0);//black
  frameRate(1000);//speed- could also put (60) here, because this is as high as the speed gets
  x = width/2; //center on the x-axis
  y = height/2; // center on the y-axis
}

function draw() {

//color
    r = random(255);//random number between 0 - 255 
    g = random(255);
    b = random(255);

  stroke(r,g,b);//random color combinations of values on the red, green, blue scale = all the colors 
//conditional statement
  if (random(0) < 255) {//if its true that 255 is higher than random(0), then the first circle will be created 

    noFill();//to make only the stroke around the circles show
    circle(width/2, height/2, x, y);//the position of the first circle (x, y, diameter), 
    //based on the values this is in the middle of the center of the canvas*/
  }

//movement
  //for loop
  for(let i = 0; i<10; i++){ //the loop stops when i < 0. The value of the
     //condition has an influence on the speed of the movements of the circles*/
x += random(-space, space); // The variable is declared in the beginning.
    //Chooses a random value between -10 and 10 to adds it to the current position 
y += random(-space, space);

  }
}
