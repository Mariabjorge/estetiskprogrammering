//buttons
let buttonReject;
let buttonAllow;
//variable for the text
let s = 'By accepting this terms and conditions your giving us access to all your personal information. You also give us the opportunity to look at you through the camera whenever we want.';
//image
function preload(){
warning=loadImage("warning.png")
}

function setup() {
  createCanvas(600,500);
  background(255);


//reject button
  buttonReject = createButton('reject all terms and conditions');
  buttonReject.style('font-size','8px')
  buttonReject.style("background","red")
  buttonReject.size(70,50);
  buttonReject.position(100,250);
  buttonReject.mousePressed(change1);

//Allow button
  buttonAllow = createButton('accept all terms and conditions :)');
  buttonAllow.style('font-size','20px')
  buttonAllow.style("background", "green")
  buttonAllow.style("color","white")
  buttonAllow.size(250,150);
  buttonAllow.position(250,200);
  buttonAllow.mousePressed(change2);


// Text
textSize(20);
fill((35, 22, 8));
text('To continue you have to accept our terms and conditions:',10,50);
//terms and conditions
fill(0);
textSize(5)
text(s, 10, 80, 500, 80); // Text wraps within text box
text(s, 10, 90, 500, 80);
text(s, 10, 100, 500, 80);
text(s, 10, 110, 500, 80);
text(s, 10, 120, 500, 80);
text(s, 10, 130, 500, 80);
text(s, 10, 140, 500, 80);

}
//if you press reject button
function change1() {
  background(255)
  fill(0);
  textSize(30);
  textStyle(BOLD);
   text('Something went wrong. Try again!', 10, 90);

}
//if you press accpet button
function change2() {
  background(0)
  fill(255);
  textSize(24);
  textStyle(BOLD);
   text('You have accepted our terms.' , 10, 90);
   textSize(15);
   text('-, 100 000 DKK. have just been just been withdrawn from your bank account!', 10, 120);
   imageMode(CENTER)
   image(warning,300,300,370,300)

//hiding the buttons when you press the "accept" button
  buttonReject.hide();
  buttonAllow.hide();

}
