## Just press the green button already

###### Runme
https://Mariabjorge.gitlab.io/estetiskprogrammering/MiniX4/

###### code: 
https://gitlab.com/Mariabjorge/estetiskprogrammering/-/blob/main/MiniX4/sketch.js

#### Description

This artwork is meant to shed light on one of the many negative aspects of data capture. When you are on the internet, it is not uncommon that you have to accept terms and conditions or cookies in order to for example visit the site you want. Since this is something that you have to do so often, for many it becomes a habit to accept these terms without even reading them through first and you are never quite sure what you really just accepted. These cookies can seem harmless after you have accepted them enough times without any consequences. You should nevertheless never be too naive when it comes to your personal data being collected online because if you’re being scammed this can lead to unfortunate consequences.

The whole page is designed in a way that tries to manipulate the user to press "accept". When you look at the page you can see a number of terms and conditions that you can choose to either accept or reject. These terms are written in such small print that it is almost impossible to read them and it is thus impossible to know what it is you accept or reject. The "Reject" button is in the color red which is often associated with something negative, while the "accept" button is green which gives a positive association. The "accept" button is also 4 times as large than the "reject button" in order to be the first thing that catches the users eye and thus tempt the person to just press the button without thinking. If you press the "reject" button, an error will occur where you will be asked to try again. This will happen every time until you have to press the "accept" button to proceed. If you do this, the screen will turn black and you will receive a message that money has been withdrawn from your bank account. The reason I chose to write this particular message is because this used to be my biggest fear as a child when I often used my parents' card information to buy things online.


#### My program and what I have learnt

In this week's miniX it has been interesting to learn how to create something that you to a greater extent can interact with. I have chosen to focus only on making buttons in this exercise. Some of the functions I have used are "createButton" to create the "accept" and "reject" buttons. To make the buttons disappear, I have used the syntax "buttonAllow.hide ()" and "buttonReject.hide" as these are the names of the buttons. To create a reaction when you press the buttons I have used the function "mouse.Pressed (change1)". This syntax allowed me to later create my own functions by typing the name I have put in parentheses after "mousePressed" ("function (change1"). In this way I could choose which changes that would happen if you press the two buttons.

#### How the program and thinking address the theme of “capture all”

My program and thinking addresses the theme of “capture all” by emphasizing how cookies are often created in a way that will manipulate the user to press "accept" by making this the most convenient choice. The choice to share personal data or not is thus not completely neutral considering that saying no in some cases can lead to not having the opportunity to e.g. visit the page you wanted or create an account. This makes it easier for people that want to trick you into giving them your sensitive data like for example your credit card information by taking advantage of how little it takes for us to press the “accept” button.


#### Cultural implications of data capture  

There are many reasons why data capturing can be harmful to the user. As smart devices are becoming more and more personalized, this also leads to more ways in which this can be abused. I would in many ways say that my smart phone knows me better than anyone else. It knows what my face looks like, it recognizes my voice, it knows where I live, my card information, all my contacts, etc. Considering all the sensitive information my smartphone holds about me, it could lead to major consequences if this fell into wrong hands.


![](datacapture-bilde.png)
![](datacapture-bilde2.png)
