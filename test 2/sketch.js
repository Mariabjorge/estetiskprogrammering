let x = 100;//x is constantly changing
let y = 300;

let spacing = 10;//inbetween the lines

function setup() {
  createCanvas(windowWidth, windowHeight);
  background(0);
  frameRate(2);

}

function draw() {
  stroke(100,200,300);
  //conditional statement
  if (random(1) < 0.5) {
    //backward slash (half of the time)
    noFill();
    circle(x, y, x+spacing, y+spacing);
  } else {
    //forward slash (half of the time)
    stroke(255,0,0)
    noFill();
    circle(x, y+spacing, x+spacing, y);
  }
  //conditional statement
  x+=spacing;
  if (x > width) {
    x = 0;
    y += spacing;
  }
}
