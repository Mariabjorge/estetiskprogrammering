## MiniX3

###### Runme:
https://Mariabjorge.gitlab.io/estetiskprogrammering/MiniX3/

###### code:
https://gitlab.com/Mariabjorge/estetiskprogrammering/-/blob/main/MiniX3/sketch.js

#### What do you want to explore and/or express?

In this MiniX I wanted to explore how I could create a throbber by making something rotate in a loop and using the functions rotate, push() and pop(). I know from experience that it can be irritating to wait on something that is loading. Therefore I wanted to make something that was fun and joyful to look at.
•	What are the time-related syntaxes/functions that you have used in your program, and why have you used them in this way?
In my program I used the functions push() and pop() to create the loop illusion. I also used the function “rotate(radians(cir));” to make the ellipses rotate around the center. Because I didn’t want the rainbow to rotate all the way around in a circle, I made the degree of each ellipse to -183 degrees  (let cir = -183/num*(frameCount%num);).  

#### How is time being constructed in computation (refer to both the reading materials and your coding)?

In the past two weeks that we have learned about loops, we have read texts about how humans and computers negotiate the experience of time. Humans have developed a time experience which is linked to natural life cycles, with influenced of both technology and social conditions. If I would make this MiniX again I would have focused a bit more on making something that has a natural cycles. When I chose to create a rainbow I kind of just focused on making something colorful and fun and it is therefore not very deep.  

#### Think about a throbber that you have encounted in digital culture, e.g. for streaming video on YouTube or loading the latest feeds on Facebook, or waiting for a payment transaction, and consider what a throbber communicates, and/or hides? How might we characterize this icon differently?

The first thing that comes to mind when I think about a throbber in digital culture is the throbber on messenger that pops up when someone is about to send you a message. Even though this throbber is quite boring to look at just consisting of three grey dots, it makes me happy to see it because I know it means that someone wants to talk to me. The massager throbber looks a lot like the one that pops up on HBO Max when the movie or show you are watching is loading. Even though these two throbbers aesthetically has a lot in common, my reaction and association to them are the opposites form each other.  

References:

Winnie Soon & Geoff Cox; 'Aesthetic Programming: A Handbook of Software Studies', 2020

![](Rainbow-throbber-picture.png)
