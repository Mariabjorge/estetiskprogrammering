function setup() {

 createCanvas(500, 500);
 frameRate (20); //the speed of the throbber
}

function draw() {
  background(120,170,250,20);//alpha
  drawElements();

}


function drawElements() {
  let num = 80;

  // push() and pop() - save and restore style/settings
  push();
  //make it in the center of the canavas
  translate(width/2, height/2);
    /* -183/num >> degree of each ellipse's movement;
  frameCount%num >> get the remainder that to know which one
  among 79 (80-1) remainding possible positions.*/
  let cir = -183/num*(frameCount%num);
  rotate(radians(cir));
  noStroke();
  fill(255, 255, 0);
  //Rainbow
  //the x parameter is the ellipse's distance from the center
  fill( 201, 73, 251)
  ellipse(90, 0, 10, 82);
  fill( 52, 23, 243)
  ellipse(100, 0, 10, 82);
  fill( 23, 243, 236)
  ellipse(110, 0, 15, 82);
  fill(97, 243, 23)
  ellipse(120, 0, 15, 82);
  fill( 240, 231, 19)
  ellipse(130, 0, 15, 82);
  fill( 240, 170, 19)
  ellipse(140, 0, 15, 82);
  fill(255,0,0)
  ellipse(150, 0, 15, 82);

  pop();
  stroke(255, 255, 0, 18);

//clouds
  fill(255)
  ellipse(400,300,80,40)
  ellipse(350,300,80,40)
  ellipse(365,320,80,40)

  ellipse(100,300,80,40)
  ellipse(150,300,80,40)
  ellipse(125,320,80,40)

  push();
  //making the text
let l="LOADING ..."
noStroke()
stroke(255,0,0);
textSize(25);
fill(random(10,200),random(50,200),random(50,200));
textAlign(100);
text(l,200,200,30,300);
pop();


}
function windowResized() {

}
