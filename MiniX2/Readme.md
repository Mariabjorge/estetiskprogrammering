###### Runme:
https://Mariabjorge.gitlab.io/estetiskprogrammering/MiniX2/

###### code:
https://gitlab.com/Mariabjorge/estetiskprogrammering/-/blob/main/MiniX2/sketch.js

### What I have made:
In this mini assignment I decided to make an emoji based on the famous artist Frida Kahlo. I spent a lot of time reflecting on how we perceive and use different emojis and the impact they have on myself and others. I also reflected on what emojis I think we are missing. After a while, I came up with the idea of making an emoji based on a role model. I decided to go for Frida Kahlo as she is a role model I find inspiring.

### The codes I have used and what I have learned:
In this week's mini X, I wanted to focus on how I could manipulate different colors and geometric shapes. In addition to this, I wanted to use variables as this is something we had been working on this week.
I obviously started by entering the codes function setup (), createCanavas () and function (draw). I used the code noStroke (), so the shapes would not have an outline. I used the codes circle (), ellipse (), rect () and arc () to create the emoji.
As I said, I wanted to include variables in this task. I thought a fun way to do this could be to make the emoji's eyes move. I use the 'let' function to create 4 different variables (x1, y1, x2 and y2), where 'x1' and 'x2' would determine one eye and 'y1' and 'y2' the other. In addition to these variables, I used the map () function. Because I wanted the emoji's eyes to follow the mouse within a certain range, I chose to make the function value 'mouseX and width' and 'mouseY and height'. This should determine which areas the eyes of the emoji should be able to move within.

In this exercise, I have gained more practice in how I can manipulate colors and geometric shapes to look the way I want. I also learned about how I can use variables and how this can be a fun way to bring something static to life.

### Wider social and cultural context:
It was a bit challenging to come up with an idea for an emoji that would have a positive effect on the user where no one would feel excluded or unrepresented. By choosing to make an emoji out of a specific role model, the user would not identify with the emoji in the same way as when it is a completely ordinary e.g. boy or girl. This will thus not lead to certain groups in society feeling excluded in regards to the emoji.
There are many reasons why I chose to make Frida Kahlo. I thought it would be a fun challenge to make her as I would have to use many different shapes and colors. Frida Kahlo is a person who is known for many things, including fighting for women's rights. An emoji that represents feminism is something I think we are missing. I also believe that making a feminine emoji with unibrows and mustache will challenge the beauty standards women face today. I think this would allow for more diversity in women's looks and make more girls and women feel seen and included.

###### References:
https://p5js.org/reference/

Winnie Soon & Geoff Cox; 'Aesthetic Programming: A Handbook of Software Studies', 2020



![](Frida_Kahlo_emoji.png)
