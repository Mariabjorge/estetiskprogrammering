function setup() {
  // put setup code here

  createCanvas(500,500); //width,height
print("hello world");
}

function draw() {

  // put drawing code here
    // put drawing code here
    background(28,186,95);

//hair
noStroke()
fill(0)
//hair left side
circle(180,70,40,50)
circle(150,80,40,50)
circle(130,100,40,50)
circle(125,120,40,50)
circle(125,120,40,50)
circle(120,150,40,50)
circle(126,183,40,50)

//hair right side
circle(270,70,40,50)
circle(300,80,40,50)
circle(320,100,40,50)
circle(325,120,40,50)
circle(325,120,40,50)
circle(327,150,40,50)
circle(320,180,40,50)

//flowers behind head-band
fill(250,165,180)
ellipse(215,30,17,25)
ellipse(215,50,17,25)
ellipse(205,40,25,17)
ellipse(225,40,25,17)
fill(225,0,0)
ellipse(215,40,10,10)

//flowers behind head-band
fill(250,165,180)
ellipse(260,30,17,25)
ellipse(260,50,17,25)
ellipse(250,40,25,17)
ellipse(270,40,25,17)
fill(225,0,0)
ellipse(260,40,10,10)

//flowers behind head-band
fill(220,165,180)
ellipse(235,40,17,25)
ellipse(235,60,17,25)
ellipse(225,50,25,17)
ellipse(245,50,25,17)
fill(225,0,0)
ellipse(235,50,10,10)

//flowers behind head-band
fill(220,165,180)
ellipse(200,40,17,25)
ellipse(200,60,17,25)
ellipse(190,50,25,17)
ellipse(210,50,25,17)
fill(255,108,150)
ellipse(200,50,10,10)

//blue head-band
fill(0,205,205)
rect(125,58,200,250,100,110,100,1000)

//hair around the head
fill(0)
rect(125,65,200,250,110,110,100,1000)

//flowers
fill(250,165,180)
ellipse(290,60,17,25)
ellipse(290,80,17,25)
ellipse(280,70,25,17)
ellipse(300,70,25,17)
fill(225,0,0)
ellipse(290,70,10,10)

//flowers
fill(220,165,180)
ellipse(215,60,17,25)
ellipse(215,80,17,25)
ellipse(205,70,25,17)
ellipse(225,70,25,17)
fill(225,0,0)
ellipse(215,70,10,10)

//flowers
fill(225,150,150)
ellipse(250,69,17,25)
ellipse(250,85,17,25)
ellipse(240,77,25,17)
ellipse(260,77,25,17)
fill(255,108,150)
ellipse(250,77,10,10)

//face
fill(250,200,100)
ellipse(225,225,170,250)

//ears
ellipse(139,229,30,50,50)
ellipse(310,229,30,50,50)

//lips
fill(255,0,0);
//bottom lips
arc(225,280,60,60,0,PI);
//upper lips
ellipse(215,278,40,30)
ellipse(235,278,40,30)
//black line deviding lips
fill(0)
rect(198,280,55,3)

//eyebrows
fill(0)
rect(155,170,130,10)

//face: to make the eyebrows look more natural
fill(250,200,100)
ellipse(185,185,70,20)
ellipse(255,185,70,20)
ellipse(220,165,40,20)

//beard
fill(46,48,51,30)
rect(177,253,15,35,100,100)
rect(257,253,15,35,100,100)
rect(225,250,30,15,100,100)
rect(193,250,30,15,100,100)

//blush in cheaks
fill(243,64,108,70)
ellipse(160,240,30,30)
ellipse(290,240,30,30)

//earrings
fill(255,216,0)
//left
circle(135,245,10)
rect(133,245,6,40,100,100)
ellipse(136,285,15,30)
//right
circle(315,245,10)
rect(310,245,6,40,100,100)
ellipse(313,285,15,30)


//right eye
fill(255);
 circle(width/1.92,height/2.5,30);

//left eye
 fill(255);
  circle(width/2.78,height/2.5,30);

//eyes with map function (left)
//map(value, start1, stop1, start2, stop2,
// Scale the mouseX value from 0 to 500 (the with of the canavas) to a range between 175 and 185
  let x1 = map(mouseX, 0, width,175,185);//left eye on the x-ax (horizontal/width)
    let y1 = map(mouseY, 0, height,198,205);// left eye on the y-ax (vertical/height)

////eyecolor
  fill(159, 95, 28)
    ellipse(x1, y1, 20, 20);//the eyes movement/position and size
//pupil
  fill(0);
  ellipse(x1, y1, 14, 14);
//glimce in eye
  fill(255);
  ellipse(x1, y1, 3, 3);


//eyes with map (right)
  let x2 = map(mouseX, 0, width,255,265);
    let y2 = map(mouseY, 0, height,198,205);

//eyecolor
  fill(159, 95, 28)
  ellipse(x2, y2, 20, 20);
//pupil
  fill(0);
  ellipse(x2, y2, 14, 14);
//glimse in eye
  fill(255);
  ellipse(x2, y2, 3, 3);


  //making the eyes more natural
  fill(250,200,100)
  rect(150,185,60,10)
  rect(230,185,60,10)

  //nose
  fill(243,181,73)
  ellipse(212,235,20,15)
  ellipse(232,235,20,15)
  ellipse(222,238,15,20)
  rect(212,180,20,60,0,0)

  //nose
  fill(0)
  ellipse(215,240,7,4)
  ellipse(230,240,7,4)
}
