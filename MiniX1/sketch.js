var x;//"var"=variable

function setup() {

  createCanvas(600,500); //width,height
print("hello world");

x = 0;
//choosing where x (the boat) is going to be in the canavas

}


//draw() runs over and over
function draw() {
  // put drawing code here
  background(130,205,255);

  //sun
  fill(255,255,70);
  ellipse(300,150,150);

//boat
fill(100,30,0)
rect(x,235,10,350,40);

fill(10)
  ellipse(x,310,100,25);

//flag on sailboat
fill(255,0,0)//Red,green,blue
rect(x,240,-35,25)

fill(255)//white
rect(x-10,240,-5,25)//"-" because then the rectangle wil appear behind "x" point
rect(x,250,-35,5)

fill(0,0,225)
rect(x-11,240,-3,25)
rect(x,251,-35,3)

x=x+1
//when x=x+1 x (the boat) is constantly moving forward in a paste of 1 pixle

	if(x>650){
x=0;
//x is moving from 0 to 650 on a loop. The reason I have chosen to make x move to 650 and not 600 (the width of the canavas) is because i want x to move all the way out of the canavas before it starts at 0 again
}

//ground
fill(255,200,90)
noStroke(); //removes the black line around the shapes
rect(0,350,600,150);

//ocean
fill(20,20,200)
rect(0,320,600,30);

//cloud 1
fill(255);//255 is white
ellipse(100,50,40,60);
ellipse(135,60,40,50);
ellipse(156,73,60,30);
ellipse(115,79,60,30);
ellipse(64,78,60,30);
ellipse(70,60,35,50);

//cloud 2
fill(255);
ellipse(500,157,48,90);
ellipse(500,190,100,40);
ellipse(556,180,70,30);
ellipse(535,169,30,50);
ellipse(460,178,80,40);
ellipse(470,160,35,50);



//cactus 1
fill(120,145,40);
//cactus main body
rect(450,180,30,200,15,15,4,4);//adding parameters in the syntax to make the corners in the rectangle rounded

//cactus left arm
rect(420,215,25,60,15,15,0,0);
rect(420,275,35,25,0,0,0,25);

//cactus right arm
rect(485,225,25,60,15,15,0,0);
rect(470,285,40,25,0,0,25,0);


//cactus 2
fill(120,165,40);
//cactus main body
rect(100,170,40,270,25,25,4,4);

//cactus left arm
rect(50,225,35,50,25,25,0,0);
rect(50,275,55,35,0,0,0,25);

//cactus right arm
rect(170,240,35,70,15,15,0,0);
rect(140,310,65,35,0,0,25,0);

//flower 1
fill(350,10,80)
ellipse(170,243,10)
ellipse(180,245,10)
ellipse(165,253,10)
ellipse(180,260,10)
ellipse(170,262,10)
ellipse(185,252,9)

//flower 1 center
fill(255,250,0)
ellipse(174,253,9)

//flower 2
fill(190,10,80)
ellipse(143,165,15)
ellipse(130,175,15)
ellipse(140,190,15)
ellipse(155,175,15)
ellipse(153,187,10)

//flower 2 center
fill(255,250,0)
ellipse(143,177,10)

//flower 3
fill(300,10,80)
ellipse(43,265,15)
ellipse(30,275,15)
ellipse(40,290,15)
ellipse(55,275,15)
ellipse(53,287,10)

//flower 3 center
fill(255,250,0)
ellipse(43,277,10)

//flower 4
fill(190,10,80)
ellipse(75,243,10)
ellipse(85,245,10)
ellipse(70,253,10)
ellipse(85,260,10)
ellipse(75,262,10)
ellipse(90,252,9)

//flower 4 center
fill(255,250,0)
ellipse(80,253,9)

//flower 5
fill(190,10,80)
ellipse(207,303,10)
ellipse(217,309,10)
ellipse(212,320,10)
ellipse(202,315,10)

//flower 5 center
fill(255,250,0)
ellipse(210,313,9)


}
