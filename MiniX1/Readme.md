## MiniX1

#### Runme:
https://Mariabjorge.gitlab.io/estetiskprogrammering/MiniX1/

#### code: 
https://gitlab.com/Mariabjorge/estetiskprogrammering/-/blob/main/MiniX1/sketch.js


##### Thoughts and feelings about my first MiniX

This mini exercise was my very first real encounter with programming. Ahead of this exercise, I felt both a little nervous and insecure when it came to programming. To begin with, I had very few expectations that I would be able to make something remotely good. I decided to take it one step at a time. As I got further and further into the process, I felt more and more confident. When the codes worked, the shapes ended up in the coordinates I wanted them to be and the colors were as expected, this gave me a feeling of mastering. Programming was suddenly not so scary anymore.


##### What I have produced:

While playing with different shapes and colors, I got the idea that I wanted to make a beach. I started to create the beach that with the help of simple shapes contained a sun, clouds, sea, sand and some cactuses with flowers. After this I wanted to increase the level a bit as I now felt that I mastered these basic shapes. I wanted to make something that could move and I thought that a boat would be something that I should be able to make that would fit well in the pictures environment.


##### The codes I have used:

During the programming process, I made extensive use of the p5.js' website and their references. I also watched a bit on YouTube, but mainly used p5.js' website. I had already learned a bit from class about how to get the project started. Although I already knew how to make some shapes and colors, it was very nice to be able to read in this website and increase my understanding around different syntaxes and parameters. The whole beach including the boat consists of the shapes 'ellipse()' and 'rect()'. I also used the code 'noStroke()' to make all the black lines around the shapes disappear. My biggest challenge during this process was making the boat move. But after looking at the "describe ()" reference, I quickly understood how I could make it move. After some trying and failing, I understood how I could make the whole boat that consisted of several figures to move in parallel with each other in a desired way.

##### How is the coding process different from, or similar to, reading and writing text?
I find the coding process quite similar to reading and writing a text. The coding language is just like any other language with different words that contain different meanings and should be used in spesific situations and contexts. When learning to use the p5.js language I kind of got the feeling that I was learning a new language, wich is kind of like a rollercoaster, both exciting and frustrating :)

##### Emotions after the exercise:

Having the opportunity to explore different codes in this exercise, has given me an increased understanding of some basic elements in p5.js. I am left with an optimism about programming, and have a greater belief that this is something I can achieve if I put enough effort into it. The text we had read during this week helped me alot. I loved the fact that they started out with the basics and helped me out so that my first ever coding experience was a sucess. 




###### References:
- https://www.youtube.com/watch?v=MFtUPJqOrdw
- https://p5js.org/reference/

![](MiniX1-bilde.png)
